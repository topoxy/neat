package neat

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"sort"
	"time"
)

type neatError struct {
	message string
}

func (e *neatError) Error() string {
	return e.message
}

func throwError(message string) error {
	return &neatError{message}
}

/////// NEAT ////////////////////////////////////////////////////

type NetworkEvaluator func(*NeuralNetwork) float64

type NEAT struct {
	population []*Genome
	species    []*Species
	generation int
	nbInputs   int
	nbOutputs  int
	evaluator  NetworkEvaluator
	maxFitness float64
	champion   *Genome
}

func newNEAT(nbInputs, nbOutputs int, evaluator NetworkEvaluator) *NEAT {
	return &NEAT{make([]*Genome, POPULATION_SIZE),
		make([]*Species, 0), 1, nbInputs, nbOutputs, evaluator, 0, nil}
}

func (neat *NEAT) addSpecies(s *Species) {
	neat.species = append(neat.species, s)
}

/**
Creates an initial population of basic random genomes
*/
func (neat *NEAT) initPopulation() {
	// We start with a general species that contains them all
	generalSpecies := newSpecies(neat.population[0])

	// We initialise the population with basic genomes
	for i := 0; i < POPULATION_SIZE; i++ {
		genome := generateBasicGenome(neat.nbInputs, neat.nbOutputs)
		neat.population[i] = genome
		generalSpecies.addGenome(genome)
	}

	neat.species = []*Species{generalSpecies}
	neat.evaluate()
}


func (neat *NEAT) reproduce() {
	// We will create the next generation's population
	// by breeding genomes in species
	newPopulation := make([]*Genome, 0)

	for _, species := range neat.species {
		// Each species is assigned a representative from the former generation
		species.updateRepresentative()

		// Elitism : only the best will reproduce and survive
		species.cutByHalf()

		if species.stagnationTime > SPECIES_STALE_LIMIT {
			continue
		}

		// Each species is offered a number of offspring
		// depending on its efficiency
		nbChild := 0

		// First, if the species is large enough, we keep the champion
		species.sortGenomes()
		if len(species.genomes) >= SPEC_MIN_CHAMP_ADD {
			localChamp := species.genomes[0].copy(false)
			localChamp.mutate()
			newPopulation = append(newPopulation, localChamp)
			nbChild += 1
		}

		for ; nbChild < species.offspringCap; nbChild++ {
			child := species.breedChild()
			newPopulation = append(newPopulation, child)
		}
	}

	for len(newPopulation) < POPULATION_SIZE {
		child := neat.population[rand.Intn(len(neat.population))].copy(true)
		child.mutate()
		newPopulation = append(newPopulation, child)
	}

	// Finally, the new population replaces the old one
	neat.population = newPopulation
}

/**
* Separate the genomes into species, depending on their distance to the different
* species representatives
 */
func (neat *NEAT) speciate() {

	// We start by clearing each species' genome pool
	for _, species := range neat.species {
		species.clear()
	}

	// Then, we go through each genome and add it to the first species compatible
	for _, genome := range neat.population {
		genomeAdded := false

		for _, species := range neat.species {
			if distanceGenomes(genome, species.representative) < DELTA_T {
				species.addGenome(genome)
				genomeAdded = true
				break
			}
		}

		// If no species was compatible, we have to create a new one
		if !genomeAdded {
			species := newSpecies(genome)
			neat.addSpecies(species)
			species.addGenome(genome)
		}
	}

	// It's time to filter the species
	survivedSpecies := make([]*Species, 0)

	for _, species := range neat.species {
		if species.stagnationTime > SPECIES_STALE_LIMIT && species.maxFitness != neat.maxFitness {
				continue
		}

		// If the species can at least produce one child, it may survive
		if len(species.genomes) > 0 && species.offspringCap >= 1 {
			// But if it staled and doesn't produce result, we don't keep
			survivedSpecies = append(survivedSpecies, species)
		}
	}
	// The survived species replaces the old ones
	neat.species = survivedSpecies
}

/**
* Calculates the fitness of the genomes and species
 */
func (neat *NEAT) evaluate() string {
	networks := make([]*NeuralNetwork, POPULATION_SIZE)

	// We will first evaluate the fitness of every genome
	for i, genome := range neat.population {
		networks[i] = inflateNetwork(genome)
		genome.fitness = networks[i].evaluate(neat.evaluator)
		if genome.fitness > neat.maxFitness {
			neat.maxFitness = genome.fitness
			neat.champion = genome
		}
	}

	totalFitness := 0.0

	// Then, we evaluate the genomes' ajusted fitness and the species' one
	for _, species := range neat.species {
		species.fitness = 0
		for _, genome := range species.genomes {
			genome.adjustedFitness = genome.fitness / float64(len(species.genomes))
			species.fitness += genome.adjustedFitness
		}
		species.updateFitnessRecords()
		species.sortGenomes()
		totalFitness += species.fitness
	}

	for _, species := range neat.species {
		// Each species will have a percentage of offspring depending on its fitness
		species.offspringCap = int(species.fitness * POPULATION_SIZE / (totalFitness))
	}

	// Finally, we sort the population by fitness
	sort.Sort(EvaluatedGenomes(neat.population))

	return "OK"
}

//////  MAIN  ////////////////////////////////////////////

/**
* TODO:
* 	- mutation resistance
*		- mutation : modifying in and out of a gene
*		- mutation : change disable mutation to toggle
*
*
*
*
*
*
*/


func Tests() {
	rand.Seed(time.Now().UTC().UnixNano())
	//rand.Seed(15)
	fmt.Println("~~ NEAT TESTING ~~")
	testSpecies()
	//testCrossing()
	//testMutations()
}

func testSpecies() {
	//reader := bufio.NewReader(os.Stdin)

	neat := newNEAT(2, 1, XORevaluator)
	neat.initPopulation()

	for i := 0; i < 40; i++ {
		fmt.Print("Step ", i, " : ")

		neat.reproduce()
		neat.speciate()
		neat.evaluate()

		for _, species := range neat.species {
			fmt.Print(",", len(species.genomes), "(", species.stagnationTime, ")")
		}

		fmt.Println()

	}

	for _, species := range neat.species {
		fmt.Println("Species : ", len(species.genomes), species.fitness, species.maxFitness)
		/*
		for _, genome := range species.genomes {
			fmt.Println("\t- Genome : ", len(genome.genes), genome.fitness, genome.adjustedFitness)
		}
		*/
	}

	/*
	fmt.Println("======= Population =========")
	for _, genome := range neat.population {
		fmt.Println("\t- Genome : ", len(genome.genes), genome.fitness, genome.adjustedFitness)
	}*/

	champion := neat.champion
	fmt.Println("\t- CHAMPION : ", len(champion.genes), champion.fitness, champion.adjustedFitness)

	network := inflateNetwork(champion)

	inputs := [][]float64{
		{0, 0},
		{0, 1},
		{1, 0},
		{1, 1},
	}

	fmt.Println("Results : ")
	for _, input := range inputs {
		res, err := network.Compute(input)
		if err == nil {
			fmt.Println(res)
		} else {
			fmt.Println("Network computation failed : ", err)
		}
	}
	DisplayNetwork(network)
}

func testCrossing() {
	genome1 := generateBasicGenome(2, 1)
	genome2 := genome1.copy(false)

	for i := 0; i < 40; i++ {
		genome1.mutate()
		genome2.mutate()
	}

	network1 := inflateNetwork(genome1)
	network2 := inflateNetwork(genome2)

	genome1.fitness = network1.evaluate(XORevaluator)
	genome2.fitness = network2.evaluate(XORevaluator)

	fmt.Println("Difference g1-g2 : ", distanceGenomes(genome1, genome2))

	reader := bufio.NewReader(os.Stdin)

	DisplayNetwork(network1)
	//genome1.Display()
	fmt.Print("Command : ")
	reader.ReadString('\n')

	DisplayNetwork(network2)
	//genome2.Display()
	fmt.Print("Command : ")
	reader.ReadString('\n')

	genome3 := crossGenomes(genome1, genome2)
	network3 := inflateNetwork(genome3)
	genome3.fitness = network3.evaluate(XORevaluator)
	DisplayNetwork(network3)

	fmt.Println("Difference g1-g3 : ", distanceGenomes(genome1, genome3))
	fmt.Println("Difference g2-g3 : ", distanceGenomes(genome2, genome3))
	fmt.Println("Difference g3-g3 : ", distanceGenomes(genome3, genome3))

	//genome3.Display()
}

func testMutations() {
	genome := generateBasicGenome(1, 1)

	network := inflateNetwork(genome)
	network.Compute([]float64{1})
	DisplayNetwork(network)

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Command : ")
	reader.ReadString('\n')

	genome.mutationAddNode()

	network = inflateNetwork(genome)
	network.Compute([]float64{1})
	DisplayNetwork(network)

	genome.Display()

	fmt.Print("Command : ")
	reader.ReadString('\n')

	genome.mutationAddNode()

	network = inflateNetwork(genome)
	network.Compute([]float64{1})
	DisplayNetwork(network)

	genome.Display()

}

func testMutationAndComputation() {
	genome := generateBasicGenome(2, 1)
	genome.Display()

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Press Enter to continue computing the network,")
	fmt.Println(" and q to stop.")

	computeGenome(genome, 10)

	for {
		fmt.Print("Command : ")
		text, _ := reader.ReadString('\n')
		if text == "q\n" {
			break
		}

		modifyGenome(genome)
		computeGenome(genome, 10)
	}
}

func modifyGenome(g *Genome) {
	g.mutate()
}

func computeGenome(g *Genome, nbSteps int) {
	network := inflateNetwork(g)

	/*
		for i := 0; i < nbSteps; i++ {
			res, err := network.Compute([]float64{1, 1})
			if err == nil {
				fmt.Println("Result : ", res)
			} else {
				fmt.Println("Network computation failed : ", err)
			}
		}
	*/

	eval := network.evaluate(XORevaluator)
	fmt.Println("Evaluation : ", eval)

	DisplayNetwork(network)
}
