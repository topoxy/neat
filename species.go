package neat

import (
	"math/rand"
	"sort"
)

/////// SPECIES /////////////////////////////////////////////////

type Species struct {
	representative *Genome
	genomes        []*Genome
	fitness        float64
	maxFitness     float64
	offspringCap   int
	stagnationTime int
}

func newSpecies(g *Genome) *Species {
	return &Species{g, make([]*Genome, 0), 0, 0, 1, 0}
}

func (s *Species) addGenome(g *Genome) {
	s.genomes = append(s.genomes, g)
}

func (s *Species) sortGenomes() {
	sort.Sort(EvaluatedGenomes(s.genomes))
}

func (s *Species) cutByHalf() {
	s.sortGenomes()

	if len(s.genomes) > 1 {
		s.genomes = s.genomes[:len(s.genomes)/2]
	}
}

func (s *Species) clear() {
	s.genomes = make([]*Genome, 0)
}

func (s *Species) breedChild() *Genome {
	genome1 := s.genomes[rand.Intn(len(s.genomes))]
	var child *Genome

	if rand.Float64() < BREED_CROSSOVER {
		genome2 := s.genomes[rand.Intn(len(s.genomes))]
		child = crossGenomes(genome1, genome2)
	} else {
		child = genome1.copy(true)
	}

	if rand.Float64() < MUTATION_CHILD {
		child.mutate()
	}

	return child
}

func (s *Species) updateFitnessRecords() {
	if s.fitness > s.maxFitness {
		if (s.maxFitness - s.fitness > RATIO_NEGLIGEABLE_IMPR*s.maxFitness) {
			s.stagnationTime = 0
		} else {
			s.stagnationTime += 1
		}
		s.maxFitness = s.fitness
	} else {
		s.stagnationTime += 1
	}
}

func (s *Species) updateRepresentative() {
	if len(s.genomes) > 0 {
		randNum := rand.Intn(len(s.genomes))
		s.representative = s.genomes[randNum].copy(false)
	}
}
