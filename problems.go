package neat

import (
	"fmt"
	"math"
	"math/rand"
)

func XORevaluator(network *NeuralNetwork) float64 {
	fitness, diff := 0.0, 0.0
	nbTimesMax := 4

	inputs := [][]float64{
		{0, 0},
		{0, 1},
		{1, 0},
		{1, 1},
	}

	outputs := []float64{0, 1, 1, 0}

	for nbTimes:=0;nbTimes<nbTimesMax;nbTimes++ {
		for _, i  := range rand.Perm(4) {
			if res, err := network.Compute(inputs[i]); err == nil {
				diff += math.Abs(outputs[i] - res[0])
			} else {
				fmt.Println("Network computation failed : ", err)
			}
		}
	}

	diff = 4*float64(nbTimesMax) - diff
	if diff < 0 {
		diff = 0
	}

	fitness = diff

	return fitness
}
