package neat

import (
	"math/rand"
)

/////// MUTATION ////////////////////////////////////////////////

/*
* MUTATIONS : => OK
* - Connection weights mutation : => OK
*     backpropagation ? random ?
* - Structural mutation :
*		- add connection : new connection with random weight => OK
*		- add node : existing connection is split => OK
* TODO: other mutations :
*		-	disable link
*		- weight bias ?
*		-
*		-
*		-
*
* INNOVATION TRACKING : => OK
* - new gene => global innovation number++ => OK
*	=> keep list of innovation during generation, give the same innov number
*		 if the same innov appears => OK
*
* GENOME MATCHING :
*	- Matching genes are inherited randomly
* - Disjoint and excess genes are inherited from the more fit parent
* - If fitnesses are equal, randomly
* -


*
 */

func (g *Genome) mutate() {
	// 80% chance of having its connection weights mutated
	if rand.Float64() < MUTA_NEW_LINK {
		g.mutationAddLink()
	}

	if len(g.genes) > 0 {
		if rand.Float64() < MUTA_WEIGHTS {
			g.mutationWeights()
		}

		if rand.Float64() < MUTA_NEW_NODE {
			g.mutationAddNode()
		}

		if rand.Float64() < MUTA_TOGGLE_LINK {
			g.mutationToggleLink()
		}
	}
}

func (g *Genome) mutationWeights() {
	for _, gene := range g.genes {
		if rand.Float64() < MUTA_UNIFORM_WEIGHT {
			// Possibility : add an age resistance to mutation
			gene.weight += rand.Float64() - 0.5
		} else {
			gene.weight = rand.Float64()*2 - 1
		}
	}
}

func (g *Genome) mutationAddLink() {
	tryCounter := 0
	mutated := false

	for !mutated && tryCounter < NB_TRY {
		numNode1 := rand.Intn(len(g.nodes))
		numNode2 := rand.Intn(len(g.nodes))
		node1 := g.nodes[numNode1]
		node2 := g.nodes[numNode2]

		// We can allow self-recurrent links
		// but not for input or output nodes
		if node1.id == node2.id && node1.nodeType != 0 {
			tryCounter += 1
			continue
		}

		// Same, we can't allow input-input and outputs-outputs links
		if sign(node1.nodeType) == sign(node2.nodeType) && node1.nodeType != 0 {
			tryCounter += 1
			continue
		}

		// We can't allow for a output node to have not-incoming links
		// or incoming link for input link
		if node1.nodeType == 1 || node2.nodeType == -1 || node2.nodeType == -2 {
			tryCounter += 1
			continue
		}

		// We can't allow for the same gene to appear twice
		if g.containsGene(node1.id, node2.id) {
			tryCounter += 1
			continue
		}

		innovGene := getInnovation(node1.id, node2.id)
		weightGene := (rand.Float64() - 0.5) * 2
		gene := newGene(node1.id, node2.id, weightGene, true, innovGene)
		g.addGene(gene)
		mutated = true
	}
}

func (g *Genome) mutationAddNode() {
	tryCounter := 0
	mutated := false

	for !mutated && tryCounter < NB_TRY {
		// First, we choose a gene to split up
		numGene := rand.Intn(len(g.genes))
		gene := g.genes[numGene]

		if !gene.activ {
			tryCounter += 1
			continue
		}

		// Then, we create a new node
		interNode := g.addNodeBetween(gene.in, gene.out)

		// Then, we create the two new genes
		innovGeneIn := getInnovation(gene.in, interNode.id)
		innovGeneOut := getInnovation(interNode.id, gene.out)
		geneIn := newGene(gene.in, interNode.id, 1, true, innovGeneIn)
		geneOut := newGene(interNode.id, gene.out, gene.weight, true, innovGeneOut)

		// Finally, we add the new genes, and deactivate the old one
		gene.activ = false
		g.addGene(geneIn)
		g.addGene(geneOut)
		mutated = true
	}
}

func (g *Genome) mutationToggleLink() {
	gene := g.genes[rand.Intn(len(g.genes))]
	gene.activ = !gene.activ
}
