package neat

import (
	"math/rand"
)

//////  GENES  ////////////////////////////////////////////

type NodeGene struct {
	id       int
	nodeType int     // -1 for input, 1 for output, 0 for hidden neuron, -2 for bias
	layer    float64 // Useful for determining the order of computation
	layer2   float64 // Useful for display
}

type Gene struct {
	in     int
	out    int
	weight float64
	activ  bool
	innov  int
}

func newNodeGene(id int, nodeType int, layer float64, layer2 float64) *NodeGene {
	gene := &NodeGene{id, nodeType, layer, layer2}
	return gene
}

func newGene(in int, out int, weight float64, activ bool, innov int) *Gene {
	gene := &Gene{in, out, weight, activ, innov}
	return gene
}

func (n *NodeGene) copy() *NodeGene {
	return newNodeGene(n.id, n.nodeType, n.layer, n.layer2)
}

func (g *Gene) copy() *Gene {
	return newGene(g.in, g.out, g.weight, g.activ, g.innov)
}

func (g *Gene) copyWithReactivationTry() *Gene {
	gene := g.copy()
	if !gene.activ {
		if rand.Float64() > MUTA_GENE_DISA_KEPT {
			gene.activ = true
		}
	}
	return gene
}

type LayeredNodeGenes []*NodeGene
type InnovNodeGenes []*NodeGene
type InnovGenes []*Gene

func (g LayeredNodeGenes) Len() int {
	return len(g)
}
func (g LayeredNodeGenes) Less(i, j int) bool {
	return g[i].layer < g[j].layer
}
func (g LayeredNodeGenes) Swap(i, j int) {
	g[i], g[j] = g[j], g[i]
}

func (g InnovNodeGenes) Len() int {
	return len(g)
}
func (g InnovNodeGenes) Less(i, j int) bool {
	return g[i].id < g[j].id
}
func (g InnovNodeGenes) Swap(i, j int) {
	g[i], g[j] = g[j], g[i]
}

func (g InnovGenes) Len() int {
	return len(g)
}
func (g InnovGenes) Less(i, j int) bool {
	return g[i].innov < g[j].innov
}
func (g InnovGenes) Swap(i, j int) {
	g[i], g[j] = g[j], g[i]
}
