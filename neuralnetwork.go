package neat

import (
	"fmt"
)

/////// NEURAL NETWORK //////////////////////////////////////////

type Neuron struct {
	value    float64
	incoming []*Gene
}

func newNeuron() *Neuron {
	neuron := &Neuron{0.0, make([]*Gene, 0)}
	return neuron
}

func (n *Neuron) addIncomingConnection(g *Gene) {
	n.incoming = append(n.incoming, g)
}

type NeuralNetwork struct {
	genome  *Genome
	neurons map[int]*Neuron
	inputs  []int
	outputs []int
}

func inflateNetwork(g *Genome) *NeuralNetwork {
	network := &NeuralNetwork{g, make(map[int]*Neuron), make([]int, 0), make([]int, 0)}

	// We start by repertoring every inputs and outputs
	for _, node := range g.nodes {
		network.neurons[node.id] = newNeuron()
		if node.nodeType == -1 {
			network.inputs = append(network.inputs, node.id)
		}
		if node.nodeType == 1 {
			network.outputs = append(network.outputs, node.id)
		}
	}

	// Then every gene is added to the concerned neurons
	for _, gene := range g.genes {
		if gene.activ {
			network.neurons[gene.out].addIncomingConnection(gene)
		}
	}

	return network
}

func (network *NeuralNetwork) Compute(inputs []float64) ([]float64, error) {
	if len(network.inputs) != len(inputs) {
		return make([]float64, 0), throwError("Incorrect number of inputs!")
	}

	network.neurons[0].value = 1 // The bias has always a value of 1

	for index, idInput := range network.inputs {
		network.neurons[idInput].value = inputs[index]
	}

	network.genome.sortByLayer()

	for _, nodeGene := range network.genome.nodes {
		neuron := network.neurons[nodeGene.id]
		sum := 0.0
		for _, incomingGene := range neuron.incoming {
			if incomingGene.activ {
				otherNeuron := network.neurons[incomingGene.in]
				sum += incomingGene.weight * otherNeuron.value
			}
		}

		if len(neuron.incoming) > 0 {
			neuron.value = sigmoid(sum)
		}
	}

	outputs := make([]float64, len(network.outputs))
	for index, idOutput := range network.outputs {
		outputs[index] = network.neurons[idOutput].value
	}

	return outputs, nil
}

func (network *NeuralNetwork) evaluate(eval NetworkEvaluator) float64 {
	return eval(network)
}

func (network *NeuralNetwork) Display() {
	fmt.Println("=== NeuralNetwork ===")
	for id, neuron := range network.neurons {
		fmt.Println("\t- Neuron ", id, " = ", neuron.value)
	}
}
