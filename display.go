package neat

import (
	"fmt"
	"github.com/llgcode/draw2d"
	"image"
	"image/color"
	"math"
	"math/rand"
)

var black = color.RGBA{0x00, 0x00, 0x00, 0xff}
var white = color.RGBA{0xff, 0xff, 0xff, 0xff}
var blue = color.RGBA{0x44, 0x44, 0xff, 0xff}
var red = color.RGBA{0xff, 0x44, 0x44, 0xff}
var font = draw2d.FontData{"luxi", draw2d.FontFamilySans, draw2d.FontStyleNormal}
var boldfont = draw2d.FontData{"luxi", draw2d.FontFamilySans, draw2d.FontStyleBold}

func DisplayNetwork(network *NeuralNetwork) {
	imageSizeX := 1600.0
	imageSizeY := 900.0
	neuronSizeX := 40.0
	neuronSizeY := 40.0
	XPadding := 60.0
	neuronColor := color.RGBA{0x44, 0x44, 0x44, 0xff}
	yStepInput := imageSizeY / float64(len(network.inputs)+2)
	yStepOutput := imageSizeY / float64(len(network.outputs)+1)
	yStepHidden := (yStepInput + yStepOutput) / 2
	dest := image.NewRGBA(image.Rect(0, 0, int(imageSizeX), int(imageSizeY)))
	gc := draw2d.NewGraphicContext(dest)

	gc.SetLineWidth(10)

	// First we calculate the Y coordinate of each neuron
	network.genome.sortByLayer()
	xvalues := make(map[int]float64)
	yvalues := make(map[int]float64)
	for _, node := range network.genome.nodes {
		switch node.nodeType {
		case -1:
			yvalues[node.id] = (node.layer2 + 1) * yStepInput
			break
		case -2:
			yvalues[node.id] = (node.layer2 + 1) * yStepInput
			break
		case 1:
			yvalues[node.id] = (node.layer2 + 1) * yStepOutput
			break
		case 0:
			//yvalues[node.id] = float64(rand.Intn(int(imageSizeY-2*neuronSizeY))) + neuronSizeY
			yvalues[node.id] = (node.layer2 + 1) * yStepHidden
			yvalues[node.id] += (rand.Float64() - 0.5) * neuronSizeY * 2
		}
		xvalues[node.id] = XPadding + node.layer*(imageSizeX-2*XPadding)
	}

	gc.SetFontData(boldfont)

	// Then, we display the links
	for _, gene := range network.genome.genes {
		if !gene.activ {
			drawArrow(gc, xvalues[gene.in], yvalues[gene.in],
				xvalues[gene.out], yvalues[gene.out], white, white, gene.weight)
		} else if gene.weight > 0 {
			drawArrow(gc, xvalues[gene.in], yvalues[gene.in],
				xvalues[gene.out], yvalues[gene.out], black, white, gene.weight)
		} else {
			drawArrow(gc, xvalues[gene.in], yvalues[gene.in],
				xvalues[gene.out], yvalues[gene.out], red, white, gene.weight)
		}
	}

	gc.SetLineWidth(10)

	// Then, we display each neuron as a rectangle
	for _, node := range network.genome.nodes {
		value := network.neurons[node.id].value
		if value > 0.75 {
			neuronColor = blue
		} else if value < 0.25 {
			neuronColor = red
		} else {
			neuronColor = white
		}
		drawRectangle(gc, xvalues[node.id], yvalues[node.id], neuronSizeX, neuronSizeY,
			neuronColor, black)
	}

	gc.SetFontData(font)
	gc.SetFontSize(18)
	gc.SetLineWidth(2)

	for id, neuron := range network.neurons {
		text := fmt.Sprintf("%d : %.2f", id, neuron.value)
		drawString(gc, xvalues[id], yvalues[id]+neuronSizeY, text, true)
	}

	gc.SetFontSize(15)

	for _, gene := range network.genome.genes {
		drawString(gc, xvalues[gene.in]+(xvalues[gene.out]-xvalues[gene.in])/3,
			yvalues[gene.in]+(yvalues[gene.out]-yvalues[gene.in])/3,
			fmt.Sprintf("%d, %.2f", gene.innov, gene.weight), false)
	}

	draw2d.SaveToPngFile("hello.png", dest)
}

func drawRectangle(gc draw2d.GraphicContext, centerX, centerY, width,
	height float64, fillColor, strokeColor color.RGBA) {
	gc.SetFillColor(fillColor)
	gc.SetStrokeColor(strokeColor)
	gc.MoveTo(centerX-width/2, centerY-height/2)
	gc.LineTo(centerX+width/2, centerY-height/2)
	gc.LineTo(centerX+width/2, centerY+height/2)
	gc.LineTo(centerX-width/2, centerY+height/2)
	gc.LineTo(centerX-width/2, centerY-height/2)
	gc.Close()
	gc.FillStroke()
}

func drawArrow(gc draw2d.GraphicContext, x1, y1, x2, y2 float64,
	strokeColor, arrowColor color.RGBA, size float64) {
	gc.SetStrokeColor(arrowColor)
	gc.SetLineWidth(math.Sqrt(math.Abs(size))*7 + 10)
	gc.MoveTo(x1, y1)
	gc.LineTo(x1+(x2-x1)*0.2, y1+(y2-y1)*0.2)
	gc.Stroke()
	gc.SetLineWidth(math.Sqrt(math.Abs(size)) * 7)
	gc.SetStrokeColor(strokeColor)
	gc.MoveTo(x1, y1)
	gc.LineTo(x2, y2)
	gc.Stroke()
}

func drawString(gc draw2d.GraphicContext, x, y float64, text string, inversed bool) {
	width := gc.FillString(text)
	if inversed {
		gc.SetLineWidth(2)
		drawRectangle(gc, x, y, width+15, 25, black, white)
		gc.SetFillColor(white)
		gc.Translate(x-width/2, y+8)
		gc.FillString(text)
		gc.Translate(-(x - width/2), -(y + 8))
	} else {
		gc.SetLineWidth(2)
		drawRectangle(gc, x, y, width+15, 25, white, black)
		gc.SetFillColor(black)
		gc.Translate(x-width/2, y+8)
		gc.FillString(text)
		gc.Translate(-(x - width/2), -(y + 8))
	}
}
