package neat

import (
	"math/rand"
)

/////// CROSSING ////////////////////////////////////////////////

func crossGenomes(g1 *Genome, g2 *Genome) *Genome {
	genomeChild := newGenome()

	g1.sortByInnov()
	g2.sortByInnov()

	cursor1, cursor2 := 0, 0
	max1, max2 := len(g1.genes), len(g2.genes)

	// We will go through each nodes list and add genes
	for cursor1 < max1 && cursor2 < max2 {
		gene1 := g1.genes[cursor1]
		gene2 := g2.genes[cursor2]

		// Matching genes
		if gene1.innov == gene2.innov {
			if rand.Float64() < 0.5 {
				genomeChild.addGene(gene1.copyWithReactivationTry())
			} else {
				genomeChild.addGene(gene2.copyWithReactivationTry())
			}
			cursor1 += 1
			cursor2 += 1
		} else { // Disjoint genes
			if gene1.innov < gene2.innov { // If g2 has a gene that g1 hasn't
				if g2.fitness >= g1.fitness { // We only keep g2's gene if it's the fittest parent
					genomeChild.addGene(gene2.copyWithReactivationTry())
				}
				cursor2 += 1
			} else { // If g1 has a gene that g1 hasn't
				if g1.fitness >= g2.fitness {
					genomeChild.addGene(gene1.copyWithReactivationTry())
				}
				cursor1 += 1
			}
		}
	}

	// Excess genes from g1
	for cursor1 < max1 && g1.fitness >= g2.fitness {
		gene1 := g1.genes[cursor1]
		genomeChild.addGene(gene1.copyWithReactivationTry())
		cursor1 += 1
	}

	// Excess genes from g2
	for cursor2 < max2 && g2.fitness > g1.fitness {
		gene2 := g2.genes[cursor2]
		genomeChild.addGene(gene2.copyWithReactivationTry())
		cursor2 += 1
	}

	usedNodes := make(map[int]bool)
	for _, gene := range genomeChild.genes {
		usedNodes[gene.in] = true
		usedNodes[gene.out] = true
	}

	// Now, we take care of adding the nodes

	// First, we add the inputs and outputs nodes
	// that are the same in both genomes
	for _, node := range g1.nodes {
		if node.nodeType != 0 {
			genomeChild.addNodeGene(node.copy())
			usedNodes[node.id] = false // So that it doesn't get added a second time
		}
	}

	// Then we need to add the nodes used by the genes
	for _, node := range g1.nodes {
		if usedNodes[node.id] {
			genomeChild.addNodeGene(node.copy())
			usedNodes[node.id] = false // So that it doesn't get added a second time
		}
	}
	for _, node := range g2.nodes {
		if usedNodes[node.id] {
			genomeChild.addNodeGene(node.copy())
			usedNodes[node.id] = false // So that it doesn't get added a second time
		}
	}

	return genomeChild
}
