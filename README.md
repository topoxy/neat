# README #

### NEAT (Neural Networks through Augmenting Topologies) ###

* **Quick summary** : 
This project is an Go implementation of the NEAT algorithm ([paper here](http://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf), 30 pages long).
It applies a genetic algorithm to generate a neural network.
In principle, you only need to provide it with the number of inputs, outputs, a fitness function, and then enough data for it to train, and voilà!

It is of course under-optimised, but it can for example solve the XOR problem in 100 generations (three times more than the original one in C++).
* **Version :** It's still a alpha buggy testy version. 

### How do I get set up? ###

* **Summary of set up : ** Use the go get command to clone the project into your machine.

* **Configuration : ** Not a lot to do I think.
* Dependencies


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* **Repo owner or admin : ** topoxy314@gmail.com 
* Other community or team contact