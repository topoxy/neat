package neat

import (
	"fmt"
	"math"
	"math/rand"
	"sort"
)

//////  GENOME  ////////////////////////////////////////////

type Genome struct {
	nodes []*NodeGene
	genes []*Gene

	fitness         float64
	adjustedFitness float64
}

func (g *Genome) findNodeGene(id int) *NodeGene {
	for _, node := range g.nodes {
		if node.id == id {
			return node
		}
	}
	return nil
}

func (g *Genome) containsGene(in, out int) bool {
	for _, gene := range g.genes {
		if gene.in == in && gene.out == out {
			return true
		}
	}
	return false
}

func (g *Genome) addGene(gene *Gene) *Gene {
	g.genes = append(g.genes, gene)
	return gene
}

func (g *Genome) addNodeGene(node *NodeGene) *NodeGene {
	g.nodes = append(g.nodes, node)
	return node
}

func (g *Genome) sortByLayer() {
	sort.Sort(LayeredNodeGenes(g.nodes))
	sort.Sort(InnovGenes(g.genes))
}

func (g *Genome) sortByInnov() {
	sort.Sort(InnovNodeGenes(g.nodes))
	sort.Sort(InnovGenes(g.genes))
}

func newGenome() *Genome {
	return &Genome{make([]*NodeGene, 0), make([]*Gene, 0), 0, 0}
}

func generateBasicGenome(nbInputs, nbOutputs int) *Genome {
	genome := newGenome()
	inputs := make([]*NodeGene, 0)
	outputs := make([]*NodeGene, 0)

	// First we create all the input and output nodes
	bias := newNodeGene(len(genome.nodes), -2, 0, 0)
	inputs = append(inputs, genome.addNodeGene(bias))

	for i := 0; i < nbInputs; i++ {
		node := newNodeGene(len(genome.nodes), -1, 0, float64(i+1))
		inputs = append(inputs, genome.addNodeGene(node))
	}
	for i := 0; i < nbOutputs; i++ {
		node := newNodeGene(len(genome.nodes), 1, 1, float64(i))
		outputs = append(outputs, genome.addNodeGene(node))
	}

	// Then we link every input at every output
	probaAddGene := 2.0 / math.Log(float64(nbInputs*nbOutputs)+2.72)

	for _, input := range inputs {
		for _, output := range outputs {
			if rand.Float64() < probaAddGene {
				weight := rand.Float64()*2 - 1
				innov := getInnovation(input.id, output.id)
				gene := newGene(input.id, output.id, weight, true, innov)
				genome.addGene(gene)
			}
		}
	}

	return genome
}

func (g *Genome) addNodeBetween(idIn, idOut int) *NodeGene {
	nodeIn := g.findNodeGene(idIn)
	nodeOut := g.findNodeGene(idOut)
	intermediaryLayer := (nodeIn.layer + nodeOut.layer) / 2.0
	intermediaryLayer2 := (nodeIn.layer2 + nodeOut.layer2) / 2.0

	node := newNodeGene(len(g.nodes), 0, intermediaryLayer, intermediaryLayer2)
	g.nodes = append(g.nodes, node)
	return node
}

func (g *Genome) copy(withTryReactivGenes bool) *Genome {
	newGenome := newGenome()

	for _, node := range g.nodes {
		newGenome.addNodeGene(node.copy())
	}

	for _, gene := range g.genes {
		if withTryReactivGenes {
			newGenome.addGene(gene.copyWithReactivationTry())
			} else {
				newGenome.addGene(gene.copy())
			}
	}

	newGenome.fitness = g.fitness
	newGenome.adjustedFitness = g.adjustedFitness

	return newGenome
}

type EvaluatedGenomes []*Genome

func (g EvaluatedGenomes) Len() int {
	return len(g)
}
func (g EvaluatedGenomes) Less(i, j int) bool {
	return g[i].fitness > g[j].fitness
}
func (g EvaluatedGenomes) Swap(i, j int) {
	g[i], g[j] = g[j], g[i]
}

func (g *Genome) Display() {
	fmt.Println("=== Genome ===")
	fmt.Println("- Nodes : ")
	for _, node := range g.nodes {
		fmt.Printf("\t -- %v\n", *node)
	}
	fmt.Println()
	fmt.Println("- Connections : ")
	for _, gene := range g.genes {
		fmt.Printf("\t -- %v\n", *gene)
	}
	fmt.Println()
}

func distanceGenomes(g1, g2 *Genome) float64 {
	max1, max2 := len(g1.genes), len(g2.genes)
	cursor1, cursor2 := 0, 0
	N, E, D, W := 0.0, 0.0, 0.0, 0.0

	g1.sortByInnov()
	g2.sortByInnov()

	N = math.Max(float64(max1), float64(max2))
	N = math.Sqrt(N + 1)

	for cursor1 < max1 && cursor2 < max2 {
		gene1 := g1.genes[cursor1]
		gene2 := g2.genes[cursor2]
		if gene1.innov == gene2.innov {
			// Matching genes
			W += math.Abs(gene1.weight - gene2.weight)
			cursor1 += 1
			cursor2 += 1
		} else if gene1.innov > gene2.innov { // Disjoint genes
			D += 1
			cursor2 += 1
		} else {
			D += 1
			cursor1 += 1
		}
	}

	W /= N

	// Excess genes
	for cursor1 < max1 {
		E += 1
		cursor1 += 1
	}

	for cursor2 < max2 {
		E += 1
		cursor2 += 1
	}

	delta := 0.0
	delta += C1 * E / N
	delta += C2 * D / N
	delta += C3 * W
	return delta
}
