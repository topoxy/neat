package neat

////// BASIC GENETICS ////////////////////////////////////

const (
	// Initial constants
	POPULATION_SIZE         = 150
	MUTATION_CHILD          = 1
	MUTA_GENE_DISA_KEPT     = 0.75
	MUTA_TOGGLE_LINK          = 0.3
	MUTA_WEIGHTS            = 0.8
	MUTA_UNIFORM_WEIGHT     = 0.9
	MUTA_NEW_NODE           = 0.03
	MUTA_NEW_LINK_SMALL_POP = 0.05
	MUTA_NEW_LINK_LARGE_POP = 0.3
	MUTA_NEW_LINK           = 0.1
	DELTA_T                 = 3.0
	SPECIES_STALE_LIMIT     = 15
	BREED_CROSSOVER         = 0.75
	INTERSPEC_MATING_RATE   = 0.001
	C1                      = 1.0
	C2                      = 1.0
	C3                      = 0.4
	NB_TRY                  = 10 // Number of times we try a loop before giving up
	SPEC_MIN_CHAMP_ADD      = 5  // Minimum size to add the champion
	RATIO_NEGLIGEABLE_IMPR  = 0.05
)

type Innovation struct {
	in       int
	out      int
	numInnov int
}

var innovations = make([]*Innovation, 0)

func getInnovation(inID, outID int) int {
	// We will first check if the innovation exists already
	for _, innov := range innovations {
		if innov.in == inID && innov.out == outID {
			// If yes, it already exists. Hence, we return the innovation number
			return innov.numInnov
		}
	}
	// Otherwise, it's a new innovation
	innovation := &Innovation{inID, outID, len(innovations)}
	innovations = append(innovations, innovation)
	return innovation.numInnov
}
