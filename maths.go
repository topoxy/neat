package neat

import (
	"math"
)

//////  MATH  ///////////////////////////////////////////

func sigmoid(x float64) float64 {
	return 1 / (1 + math.Exp(-4.9*x))
}

func sign(x int) int {
	if x > 0 {
		return 1
	} else if x < 0 {
		return -1
	} else {
		return 0
	}
}
